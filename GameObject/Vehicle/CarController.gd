extends Spatial

var Accelerating = false
var Reversing = false

export var CollisionShapes = []

var IsGrounded = false

var STEER_FORCE = 10

# TODO: Move to config file and such
var car_data = {
	stats = {
		accel = 15000, # Max torque
		speed = 100    # Top speed
	},
	wheels = [
		{ driving = true,  steering = false, grip = 1, offset = Vector3(-0.80, 0.85, -1.90) }, # RR
		{ driving = true,  steering = false, grip = 1, offset = Vector3( 0.80, 0.85, -1.90) }, # RL
		{ driving = true,  steering = true,  grip = 1, offset = Vector3( 0.80, 0.85,  1.50) }, # FL
		{ driving = true,  steering = true,  grip = 1, offset = Vector3(-0.80, 0.85,  1.50) }  # FR
	]
}


func _ready():
	# Initialise wheels
	for wheel_index in range(len(car_data.wheels)):
		get_node("Wheel"     +str(wheel_index)).friction         = car_data.wheels[wheel_index].grip
		get_node("WheelJoint"+str(wheel_index)).transform.origin = car_data.wheels[wheel_index].offset
		get_node("Wheel"     +str(wheel_index)).transform.origin = car_data.wheels[wheel_index].offset
		get_node("WheelJoint"+str(wheel_index)).set_exclude_nodes_from_collision(!get_node("WheelJoint"+str(wheel_index)).get_exclude_nodes_from_collision())
		get_node("WheelJoint"+str(wheel_index)).set_exclude_nodes_from_collision(!get_node("WheelJoint"+str(wheel_index)).get_exclude_nodes_from_collision())
		get_node("WheelJoint"+str(wheel_index)).set_param_y(Generic6DOFJoint.PARAM_ANGULAR_UPPER_LIMIT, 0)
		get_node("WheelJoint"+str(wheel_index)).set_param_y(Generic6DOFJoint.PARAM_ANGULAR_LOWER_LIMIT, 0)
		get_node("WheelJoint"+str(wheel_index)).set_param_x(Generic6DOFJoint.PARAM_ANGULAR_MOTOR_FORCE_LIMIT, car_data.stats.accel)
	
	# Reset steering to zero
	steer(0)
	
	# Enable all motors
	toggle_motors(true)

func _physics_process(_delta: float):
	
	# Input handling
	# TODO: Move to generic player input controller
	
	if Input.is_action_pressed("ui_accept"):
		#print("brake")
		handbrake()
		
	if Input.is_action_pressed("ui_down"):
		reverse(Input.get_action_strength("ui_down"))
		
	if Input.is_action_just_pressed(("ui_down")):
		Reversing = true
		
	if Input.is_action_just_released("ui_down"):
		reverse(0)
		Reversing = false

	if Input.is_action_pressed("ui_up"):
		accelerate(Input.get_action_strength("ui_up"))
		Accelerating = true
		
	if Input.is_action_just_released("ui_up"):
		accelerate(0)
		Accelerating = false
	
	if Input.is_action_pressed("ui_left"):
		steer(-Input.get_action_strength("ui_left"))

	if Input.is_action_pressed("ui_right"):
		steer(Input.get_action_strength("ui_right"))

	if Input.is_action_just_released("ui_right"):
		steer(0)

	if Input.is_action_just_released("ui_left"):
		steer(0)
	
func toggle_motors(state: bool):
	# Set motors to enabled
	for wheel_index in range(len(car_data.wheels)):
		# Check if wheel is motorised
		if car_data.wheels[wheel_index].driving:
			# Set motor flag to provided state
			get_node("WheelJoint"+str(wheel_index)).set_flag_x(
				Generic6DOFJoint.FLAG_ENABLE_MOTOR,
				state
			)
	
func steer(amount: float):
	# TODO: Define angle for each wheel?
	# TODO: Alternatively, whole car, if we decide to never have 4 wheel steering
	# TODO: If we do decide to have 4 wheel steering, we also need a flag for inversion
	# TODO: Or just use inverse angle
	# Also I have no idea what the hell these units are
	# Radians maybe, either way this is apparently 30°
	var angle = 0.523599
	
	for wheel_index in range(len(car_data.wheels)):
		# Check if wheel is steerable
		if car_data.wheels[wheel_index].steering:
			# Set angle param state
			# Have small deadzone
			# TODO: Make configurable
			if amount < 0.1 and amount > -0.1:
				get_node("WheelJoint"+str(wheel_index)).set_param_y(Generic6DOFJoint.PARAM_ANGULAR_UPPER_LIMIT, 0)
				get_node("WheelJoint"+str(wheel_index)).set_param_y(Generic6DOFJoint.PARAM_ANGULAR_LOWER_LIMIT, 0)

			if amount > 0:
				get_node("WheelJoint"+str(wheel_index)).set_param_y(Generic6DOFJoint.PARAM_ANGULAR_UPPER_LIMIT, angle * amount)
			elif amount < 0:
				get_node("WheelJoint"+str(wheel_index)).set_param_y(Generic6DOFJoint.PARAM_ANGULAR_LOWER_LIMIT, angle * amount)
					
			get_node("WheelJoint"+str(wheel_index)).set_param_y(Generic6DOFJoint.PARAM_ANGULAR_MOTOR_TARGET_VELOCITY, amount * STEER_FORCE)


func accelerate(amount: float):
	if amount == 0:
		# Disable motors when not needed
		toggle_motors(false)
		
	else:
		# Re-enable motors
		# This checks if wheels are motorised so no need to check here
		toggle_motors(true)
	
		# Set top speeds for each wheel
		for wheel_index in range(len(car_data.wheels)):
				# Set motor target velocity
				# TODO: Simulate gears and torque curve somewhere in here
			get_node("WheelJoint"+str(wheel_index)).set_param_x(
				Generic6DOFJoint.PARAM_ANGULAR_MOTOR_TARGET_VELOCITY,
				-amount * car_data.stats.speed
			)
	
	
func reverse(amount: float):
	if amount == 0:
		# Disable motors when not needed
		toggle_motors(false)
		
	else:
		# Re-enable motors
		# This checks if wheels are motorised so no need to check here
		toggle_motors(true)
		
		# Set top speeds for each wheel
		# For now this is identical to acceleration
		# TODO: Implement special reverse behaviour (e.g. capped top speed)
		# TODO: Alternatively integrate into acceleration function
		for wheel_index in range(len(car_data.wheels)):
			# Set motor target velocity
			# TODO: Simulate gears and torque curve somewhere in here
			get_node("WheelJoint"+str(wheel_index)).set_param_x(
				Generic6DOFJoint.PARAM_ANGULAR_MOTOR_TARGET_VELOCITY,
				amount * car_data.stats.speed
			)
	
	
func handbrake():
	# Enable motors
	toggle_motors(true)
	
	# TODO: Dedicated handbraking wheels
	# Set all motors to target velocity zero
	for wheel_index in range(len(car_data.wheels)):
		get_node("WheelJoint"+str(wheel_index)).set_param_x(Generic6DOFJoint.PARAM_ANGULAR_MOTOR_TARGET_VELOCITY, 0)
